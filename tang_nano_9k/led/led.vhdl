-- This VHDL was converted from Verilog using the
-- Icarus Verilog VHDL Code Generator 11.0 (stable) (v11_0)

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Generated from Verilog module led (led.sv:4)
entity led is
  port (
    clk : in std_logic;
    led_sig : out unsigned(5 downto 0);
    reset : in std_logic
  );
end entity; 

-- Generated from Verilog module led (led.sv:4)
architecture from_verilog of led is
  signal led_sig_Reg : unsigned(5 downto 0);
  signal counter : std_logic;  -- Declared at led.sv:5
begin
  led_sig <= led_sig_Reg;
  
  -- Generated from always process in led (led.sv:7)
  process is
  begin
    wait until falling_edge(reset) or rising_edge(clk);
    if (not reset) = '1' then
      counter <= '0';
    else
      wait for 0 ns;  -- Read target of blocking assignment (led.sv:10)
      if (unsigned'("00000000000000000000000") & counter) < X"cdfe5f" then
        counter <= '1';
      else
        counter <= '0';
      end if;
    end if;
  end process;
  
  -- Generated from always process in led (led.sv:16)
  process (clk, reset) is
  begin
    if (not reset) = '1' then
      led_sig_Reg <= "010101";
    elsif rising_edge(clk) then
      if (unsigned'("00000000000000000000000") & counter) = X"cdfe5f" then
        led_sig_Reg <= "111111";
      else
        led_sig_Reg <= led_sig_Reg;
      end if;
    end if;
  end process;
end architecture;

