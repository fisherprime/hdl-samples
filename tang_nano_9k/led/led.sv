// LED blinker at 1Hz using an enable generator.
//
// LEDs are active low.
/// REF: sipeed/TangNano-9K-example
`timescale 1ns/1ns

module led(input clk, input reset, output reg[5:0] led);
   reg counter;

   always @(posedge clk or negedge reset) begin
      if (!reset)
	counter = 24'd0;
      else if (counter < 24'd1349_9999) // 0.5s delay
	counter += 1'd1;
      else
	counter = 24'd0;
   end

   always @(posedge clk or negedge reset) begin
      if (!reset)
	led <= 6'b000001;
      else if (counter == 24'd1349_9999)
	// Shift light.
	led[5:0] <= {led[4:0], led[5]};
      else
	led <= led;
   end
endmodule
