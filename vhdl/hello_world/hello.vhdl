-- REF: https://ghdl.github.io/ghdl/quick_start/simulation/hello/index.html
use std.textio.all;

entity hello is
end hello;

architecture behaviour of hello is
begin
  process
    variable l : line;
  begin
    write(l, string'("Hello world!"));
    writeline(output, l);
    wait;
  end process;
end behaviour;
