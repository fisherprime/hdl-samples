library ieee;
use ieee.std_logic_1164.all;

entity my_and is
	port (i0, i1 : in std_logic; o : out std_logic);
end my_and;

architecture behaviour of my_and is
begin
	o <= i0 and i1;
end behaviour;
