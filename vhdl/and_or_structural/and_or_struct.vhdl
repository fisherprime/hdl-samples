library ieee;
use ieee.std_logic_1164.all;

entity and_or_struct is
	port (a, b, d, e : in std_logic; g : out std_logic);
end and_or_struct;

architecture structure of and_or_struct is
	-- Declaration of my_and component.
	component my_and is
		port (i0, i1 : in std_logic; o : out std_logic);
	end component;

	-- Declaration of my_or component.
	component my_or is
		port(i0, i1 : in std_logic; o : out std_logic);
	end component;

	-- Specify entity bound in the component.
	for a1 : my_and use entity work.my_and;
	for a2 : my_and use entity work.my_and;
	for o1 : my_or use entity work.my_or;

	-- Wires to connect components.
	signal c: std_logic;
	signal f: std_logic;
begin
	a1 : my_and port map(i0 => a, i1 => b, o => c);
	a2 : my_and port map(i0 => d, i1 => e, o => f);
	o1 : my_or port map(i0 => c, i1 => f, o => g);
end structure;
