library ieee;
use ieee.std_logic_1164.all;

entity my_or is
	port (i0, i1 : in std_logic; o : out std_logic);
end my_or;

architecture behaviour of my_or is
begin
	o <= i0 or i1;
end behaviour;
