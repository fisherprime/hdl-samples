-- Adder testbench
entity adder_tb is
end adder_tb;

architecture behaviour of adder_tb is
  -- Declaration of component to instantiate.
  component adder
    port(i0, i1, ci : in bit; s, co : out bit);
  end component;

  -- Specify entity bound in the component.
  for adder_o              : adder use entity work.adder;
  signal i0, i1, ci, s, co : bit;

begin
  -- Component instantiation.
  --
  -- Map component ports to the entity's ports.
  adder_o : adder port map(i0 => i0, i1 => i1, ci => ci, s => s, co => co);

  -- Testbech process.
  process
    type pattern_type is record
      -- The inputs.
      i0, i1, ci : bit;
      -- The outputs.
      s, co      : bit;
    end record;

    -- Patterns to apply.
    type pattern_array is array (natural range <>) of pattern_type;
    constant patterns : pattern_array :=
      (('0', '0', '0', '0', '0'),
       ('0', '0', '1', '1', '0'),
       ('0', '1', '0', '1', '0'),
       ('0', '1', '1', '0', '1'),
       ('1', '0', '0', '1', '0'),
       ('1', '0', '1', '0', '1'),
       ('1', '1', '0', '0', '1'),
       ('1', '1', '1', '1', '1'));

  begin
    -- For each pattern.
    for i in patterns'range loop
      -- Set the inputs.
      i0 <= patterns(i).i0;
      i1 <= patterns(i).i1;
      ci <= patterns(i).ci;

      -- Await stabilization.
      wait for 1 fs;

      -- Check outputs.
      assert s = patterns(i).s
        report "invalid sum: " & bit'image(s) & ", expected: " & bit'image(patterns(i).s) severity error;

      assert co = patterns(i).co
        report "invalid carry out: " & bit'image(s) & ", expected: " &
        bit'image(patterns(i).co) severity error;

      report "(" & bit'image(i0) & ", " & bit'image(i1) & ", " & bit'image(ci) & ", " &
        bit'image(s) & ", " & bit'image(co) & ")";
    end loop;

    report "test complete";
    -- Same thing as above.
    -- assert false report "test complete" severity note;

    wait;
  end process;
end behaviour;
