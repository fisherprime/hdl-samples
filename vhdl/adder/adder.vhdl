entity adder is
  port (i0, i1, ci : in bit; s, co : out bit);
end adder;

architecture behaviour of adder is
begin
  -- NOTE: VHDL assignments are concurrent.
  -- Compute the s.
  s  <= i0 xor i1 xor ci;
  -- Compute the c.
  co <= (i0 and i1) or (i0 and ci) or (i1 and ci);
end behaviour;
