library ieee;
use ieee.std_logic_1164.all;

entity and_or_behav is
  port(a, b, d, e : in std_logic; g : out std_logic);
end and_or_behav;

architecture behaviour of and_or_behav is
begin
  g <= (a and b) or (d and e);
end behaviour;
