library ieee;
use ieee.std_logic_1164.all;

entity and_or_behav_tb is
end and_or_behav_tb;

architecture behaviour of and_or_behav_tb is
  component and_or_behav is
    port (a, b, d, e : in std_logic; g : out std_logic);
  end component;

  for and_or_1               : and_or_behav use entity work.and_or_behav;
  signal a, b, c, d, e, f, g : std_logic;

begin
  -- Intantiate component.
  and_or_1 : and_or_behav port map(a => a, b => b, d => d, e => e, g => g);

  -- Testbech process.
  process
    type pattern_type is record
      -- Input.
      a, b, d, e : std_logic;
      -- Signals.
      c, f       : std_logic;
      -- Output.
      g          : std_logic;
    end record;

    type pattern_array is array (natural range<>) of pattern_type;
    constant patterns : pattern_array :=
      (('0', '0', '0', '0', '0', '0', '0'),
       ('0', '0', '0', '1', '0', '0', '0'),
       ('0', '0', '1', '0', '0', '0', '0'),
       ('0', '0', '1', '1', '0', '1', '1'),
       ('0', '1', '0', '0', '0', '0', '0'),
       ('0', '1', '0', '1', '0', '0', '0'),
       ('0', '1', '1', '1', '0', '1', '1'),
       ('1', '0', '0', '0', '0', '0', '0'),
       ('1', '0', '0', '1', '0', '0', '0'),
       ('1', '0', '1', '0', '0', '0', '0'),
       ('1', '0', '1', '1', '0', '1', '1'),
       ('1', '1', '0', '0', '1', '0', '1'),
       ('1', '1', '0', '1', '1', '0', '1'),
       ('1', '1', '1', '0', '1', '0', '1'),
       ('1', '1', '1', '1', '1', '1', '1'));

  begin
    for i in patterns'range loop
      a <= patterns(i).a;
      b <= patterns(i).b;
      d <= patterns(i).d;
      e <= patterns(i).e;

      -- Await stabilization.
      wait for 1 fs;

      -- Check outputs.
      assert g = patterns(i).g
        report "invalid result (g): " & std_logic'image(g) & ", expected: " &
        std_logic'image(patterns(i).g) severity error;

      report "(" & std_logic'image(a) & ", " & std_logic'image(b) & ", " & std_logic'image(d) & ", " &
        std_logic'image(e) & ", " & std_logic'image(g) & ")";
    end loop;

    report "test complete";

    wait;
  end process;
end behaviour;
