// Specify time_unit & time_precision for simulations.
//
// `time_unit` is the measurement of simulation times.
// `time_precision` specifies the rounding of delay values.
`timescale 1ns/1ns

module heartbeat (output clk);
   reg c;
   integer clk_delay = 10;

   assign clk = c;

   always begin
      // Alternatively.
      // always @(*) begin
      c <= 0;

      // Delay then execute.
      #(clk_delay/2) c <= 1;

      // Delay before exit.
      #(clk_delay/2);
   end
endmodule
