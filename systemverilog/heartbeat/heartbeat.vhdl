-- This VHDL was converted from Verilog using the
-- Icarus Verilog VHDL Code Generator 11.0 (stable) (v11_0)

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Generated from Verilog module heartbeat (heartbeat.sv:6)
entity heartbeat is
  port (
    clk : out std_logic
  );
end entity; 

-- Generated from Verilog module heartbeat (heartbeat.sv:6)
architecture from_verilog of heartbeat is
  signal clk_Reg : std_logic;
  signal clk_delay : signed(31 downto 0) := X"0000000a";  -- Declared at heartbeat.sv:7
begin
  clk <= clk_Reg;
  -- Removed one empty process
  
  
  -- Generated from always process in heartbeat (heartbeat.sv:9)
  process is
  begin
    clk_Reg <= '0';
    wait for To_Integer(unsigned((Resize((clk_delay / X"00000002"), 64) * X"0000000000000001"))) * 1 ns;
    clk_Reg <= '1';
    wait for To_Integer(unsigned((Resize((clk_delay / X"00000002"), 64) * X"0000000000000001"))) * 1 ns;
  end process;
end architecture;

