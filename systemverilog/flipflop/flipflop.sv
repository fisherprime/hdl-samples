`timescale 1ns/1ns

// Flip flops are used to cross clock domains.

// D flip flop.
module d_ff (input clk, d, output reg q);
   always @(posedge clk)
     q <= d;
endmodule


// 8-bit D flip flop.
module d_multi_ff (input clk, input [7:0] d, output [7:0] q);
   reg [7:0] tmp;
   assign q = tmp;

   always @(posedge clk)
     tmp <= d;
endmodule

// 8-bit D flip flop with active high synchronous reset.
//
// Posive edge triggered.
module d_reset_ff (input clk, reset, input [7:0] d, output [7:0] q);
   reg [7:0] tmp;
   assign q = tmp;

   always @(posedge clk)
     begin
	if (reset)
	  tmp <= 0;
	else
	  tmp <= d;
     end
endmodule


// 8-bit D flip flop with active high synchronous reset value.
//
// Negative edge triggered.
module d_reset_val_ff (input clk, reset, input [7:0] d, output [7:0] q);
   reg [7:0] tmp;
   assign q = tmp;

   always @(negedge clk)
     begin
	if (reset)
	  tmp <= 52'h34;
	else
	  tmp <= d;
     end
endmodule

// 8-bit D flip flop with active high asynchronous reset.
//
// Positive edge triggered.
module d_async_reset_ff (input clk, areset, input [7:0] d, output [7:0] q);
   reg [7:0] tmp;
   assign q = tmp;

   always @(posedge clk, posedge areset) begin
      if (areset == 1'b1)
	tmp <= 0;
      else
	tmp <= d;
   end

endmodule

// 16-bit D flip flop with byte enable control & active low synchronous reset.
//
// Positive edge triggered.
module d_nreset_enable_ff (input clk, resetn, input [1:0] byteena, input [15:0] d, output [15:0] q);
   reg [15:0] tmp;
   assign q = tmp;

   always @(posedge clk) begin
      if (byteena[1])
	if (~resetn)
	  tmp[15:8] = 0;
	else
	  tmp[15:8] = d[15:8];

      if (byteena[0])
	if (~resetn)
	  tmp[7:0] = 0;
	else
	  tmp[7:0] = d[7:0];
   end

endmodule

// A D latch.
//
// A latch stores a value when not enabled; behaving as a wire then enabled.
module d_latch(input d, ena, output q);
   reg tmp;
   assign q = t;

   always @(ena)
     if (ena)
       t <= d;
endmodule
