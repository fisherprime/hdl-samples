`timescale 1ns/1ns

// Full adder implementation.
module full_adder (input a, b, cin, output sum, cout/*AUTOARG*/);
   assign sum = a ^ b ^ cin;
   assign cout = (a & b) || (a & cin) || (b & cin);
endmodule

// Half adder implementation.
module half_adder(input a, b, output sum, cout/*AUTOARG*/);
   assign sum = a ^ b;
   assign cout = (a & b);
endmodule
