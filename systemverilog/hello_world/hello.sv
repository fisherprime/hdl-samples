module hello;
   // Single statement _initial_ blocks do not require the _begin_ & _end_ keywords.
   initial
     // display terminates with a newline.
     $display("Hello world!");

   // write does not terminate with a newline.
   // $write("Hello world!");
endmodule
