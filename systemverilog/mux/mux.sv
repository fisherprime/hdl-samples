`timescale 1ns/1ns

// 2->1 mux.
module mux2 (input a, b, sel, output out);
   assign out = (~sel & a) || (sel & b);
endmodule

// 2->1 100-bit wide mux.
module mux2_100 (input [99:0] a, b, input sel, output [99:0] out);
   reg [99:0] t;
   assign out = t;

   always @(*)
     begin
	if (sel)
	  t <= b;
	else
	  t <= a;
     end
endmodule // mux2_bus

// 9->1 16-bit wide mux.
module mux9_16(
	       input [15:0]  a, b, c, d, e, f, g, h, i,
	       input [3:0]   sel,
	       output [15:0] out );

   reg [15:0]		     t;
   assign out = t;

   always @(*)
     begin
	case (sel)
	  0: t <= a;
	  1: t <= b;
	  2: t <= c;
	  3: t <= d;
	  4: t <= e;
	  5: t <= f;
	  6: t <= g;
	  7: t <= h;
	  8: t <= i;
	  9: t <= 65535'hffff;
	  10: t <= 65535'hffff;
	  11: t <= 65535'hffff;
	  12: t <= 65535'hffff;
	  13: t <= 65535'hffff;
	  14: t <= 65535'hffff;
	  15: t <= 65535'hffff;
	endcase
     end
endmodule

// 256-bit->1-bit mux.
module mux_256(
	       input [255:0] in,
	       input [7:0]   sel,
	       output	     out );

   reg			     t;
   assign out = t;

   always @(*)
     // Sel contains a selector from 0-255.
     t <= in[sel];
endmodule

// 256-bit->4-bit mux.
module mux_1024(
		input [1023:0] in,
		input [7:0]    sel,
		output [3:0]   out );

   reg [3:0]		       t;
   integer		       upper, lower;

   assign out = t;

   always @(*)
     begin
	// Using variable part-select.
	//
	// The width must remain constant.
	//
	// [<start_bit> +: width]
	// in[x +: y] == in[(x + y) -1: x]
	// in[sel * 4 +: 4] => in[(0 * 4) - 1: 0 * 4], in[(1 * 4) - 1: 1 * 4]
	t <= in[sel*4 +: 4];
     end
endmodule
