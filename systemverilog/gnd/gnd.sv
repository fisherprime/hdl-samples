`timescale 1ns/1ns

module ground (output out);
   assign out = 0;
   // Alternatively.
   // assign out = 1'b0;
endmodule
